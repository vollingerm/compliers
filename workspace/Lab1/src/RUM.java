import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;




public class RUM {
	public interface Node {
		public <T> T accept(Visitor<T> visitor);
	}
	public interface Visitor<T> {
		public T visit(Left node);
		public T visit(ProcedureInvocation node);
		public T visit(ProcedureDefinition node);
		public T visit(Right node);
		public T visit(Increment node);
		public T visit(Decrement node);
		public T visit(Input node);
		public T visit(Output node);
		public T visit(Loop node);
		public T visit(Program node);
		public T visit(Sequence node);
		public T visit(Repetition node);
		public T visit(rumString node);
		public T visit(Breakpoint node);
	}
	/* Define various node types */
	public static class Left implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Right implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Increment implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Breakpoint implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Decrement implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Input implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Output implements Node {
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Loop implements Node {
		Node child;
		public Loop (Node child) {
			this.child = child;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Program implements Node {
		Node child;
		public Program (Node child) {
			this.child = child;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class Sequence implements Node {
		Node[] children;
		public Sequence(Node...children) {
			this.children = children;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class ProcedureDefinition implements Node{
		Node child;
		public ProcedureDefinition (Node child) {
			this.child = child;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	public static class ProcedureInvocation implements Node{
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	
	//NOT CORRECT Think of using loop, it is just adition to that
	public static class Repetition implements Node{
		int rep;
		char command;
		public Repetition (int rep, char command) {
				this.rep = rep;
				this.command = command;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Maybe correct
	public static class rumString implements Node{
		String child;
		public rumString (String temp1) {
			this.child = temp1;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	/* Define the visitors themselves */
	public static class Printer implements Visitor<Void> {

		@Override
		public Void visit(Left node) {
			System.out.print('<');
			return null;
		}

		@Override
		public Void visit(Right node) {
			System.out.print('>');
			return null;
		}

		@Override
		public Void visit(Increment node) {
			System.out.print('+');
			return null;
		}

		@Override
		public Void visit(Decrement node) {
			System.out.print('-');
			return null;
		}

		@Override
		public Void visit(Input node) {
			System.out.print(',');
			return null;
		}

		@Override
		public Void visit(Output node) {
			System.out.print('.');
			return null;
		}

		@Override
		public Void visit(Loop node) {
			System.out.print('[');
			node.child.accept(this);
			System.out.print(']');
			return null;
		}

		@Override
		public Void visit(Program node) {
			node.child.accept(this);			
			return null;
		}

		@Override
		public Void visit(Sequence node) {
			for (Node child : node.children) {
				child.accept(this);
			}
			return null;
		}

		@Override
		public Void visit(ProcedureDefinition node) {
			System.out.print('(');
			node.child.accept(this);
			System.out.print(')');
			return null;
		}
		
		@Override
		public Void visit(ProcedureInvocation node){
			System.out.print(':');
			return null;
		}
		
		@Override
		public Void visit(Repetition node) {
			
			System.out.print(""+node.rep);
			switch (node.command) {
			case '>': System.out.print("pointer++"); break;
			case '<': System.out.print("pointer--"); break;
			case '+': System.out.print("cell[pointer]++"); break;
			case '-': System.out.print("cell[pointer]--"); break;
			case '.': System.out.print("."); break;
			case ',': System.out.print(",");break;
		}
			return null;
		}

		@Override
		public Void visit(rumString node) {
			System.out.print('"');
			System.out.print(node.child.toString());
			System.out.print('"');
			
			return null;
		}

		@Override
		public Void visit(Breakpoint node) {
			//print all relevent knowledge
			System.out.println(" This is a breakpoint, here is the known data");
			//System.out.println("")
			return null;
		}
		
	}
	public static class CompiletoJava implements Visitor<Void> {
		int proc = 0;
		int arraypointer =0;
		String methodArray[] = new String[256];
		int methodpointer =0;
		boolean inmethod = false;
		
		@Override
		public Void visit(Left node) {
			if(inmethod){
				methodArray[methodpointer] +="arraypointer--;\n";
			}else
				System.out.println("arraypointer--;");
			arraypointer--;
			return null;
		}

		@Override
		public Void visit(Right node) {
			if(inmethod){
				methodArray[methodpointer] +="arraypointer++;\n";
			}else
				System.out.println("arraypointer++;");
			arraypointer ++;
			return null;
		}

		@Override
		public Void visit(Increment node) {
			if(inmethod){
				methodArray[methodpointer] +="array[arraypointer]++;\n";
			}else
				System.out.println("array[arraypointer]++;");
			return null;
		}

		@Override
		public Void visit(Decrement node) {
			if(inmethod){
				methodArray[methodpointer] +="array[arraypointer]--;\n";
			}else
				System.out.println("array[arraypointer]--;");
			return null;
		}

		@Override
		public Void visit(Input node) {
			if(inmethod){
				methodArray[methodpointer] ="array[arraypointer] = System.in;\n";
			}else
				System.out.println("array[arraypointer] = System.in;");
			return null;
		}

		@Override
		public Void visit(Output node) {
			if(inmethod){
				methodArray[methodpointer] +="System.out.print((char) array[arraypointer]);\n";
			}else
				System.out.println("System.out.print((char) array[arraypointer]);");
			return null;
		}

		@Override
		public Void visit(Loop node) {
			if(inmethod){
				methodArray[methodpointer] +="While(array["+arraypointer+"] != 0) {\n";
				node.child.accept(this);
				methodArray[methodpointer] +="}\n";
			}else{
				System.out.println("While(array["+arraypointer+"] != 0) {");
				node.child.accept(this);
				System.out.println('}');
			}
			return null;
		}

		@Override
		public Void visit(Program node) {
			System.out.println("public class Myprogram {");
			System.out.println("public void main() {");
			System.out.println("int arraypointer = 0;");
			System.out.println("int array[] = new int[30000]");
			node.child.accept(this);		
			System.out.println("return null; \n }");
			for(int j= 0; j <256; j++){
				if(methodArray[j] != null){
					System.out.println(methodArray[j].toString());
				}
			}
			System.out.println("\n }");
			return null;
		}

		@Override
		public Void visit(Sequence node) {
			for (Node child : node.children) {
				child.accept(this);
			}
			System.out.println("");
			return null;
		}

		@Override
		// needs to be outside of a main, pass reference cell pointer(char**)
		public Void visit(ProcedureDefinition node) {
			inmethod = true;
			methodpointer = arraypointer;
			methodArray[arraypointer] += "\n public void method" + arraypointer + "(){\n";
			node.child.accept(this);
			methodArray[arraypointer] +="\n}";
			inmethod = false;
			return null;
		}
		
		@Override
		public Void visit(ProcedureInvocation node){
			System.out.println("method"+ arraypointer + "();");
			return null;
		}
		
		@Override
		public Void visit(Repetition node) {
			if(inmethod){
				methodArray[methodpointer] +="For(int i = 0; i < "+ node.rep +"; i++){\n";
				switch (node.command) {
					case '>': methodArray[methodpointer] +="arraypointer++;\n"; break;
					case '<': methodArray[methodpointer] +="arraypointer--;\n"; break;
					case '+': methodArray[methodpointer] +="array[arraypointer]++;\n"; break;
					case '-': methodArray[methodpointer] +="array[arraypointer]--;\n"; break;
					case '.': methodArray[methodpointer] +="System.out.print(\"array[arraypointer]\");\n"; break;
					case ',': methodArray[methodpointer] +="array[arraypointer] = System.in;\n";break;
				}
				methodArray[methodpointer] +="}\n";
			}else{
				System.out.println("For(int i = 0; i < "+ node.rep +"; i++){");
				switch (node.command) {
					case '>': System.out.println("arraypointer++;"); break;
					case '<': System.out.println("arraypointer--;"); break;
					case '+': System.out.println("array[arraypointer]++;"); break;
					case '-': System.out.println("array[arraypointer]--;"); break;
					case '.': System.out.println("System.out.print(\"array[arraypointer]\");"); break;
					case ',': System.out.println("array[arraypointer] = System.in;");break;
				}
				System.out.println('}');
			}
			//ToDo the amount of repetitions, think of calling loop
			return null;
		}

		@Override
		public Void visit(rumString node) {
			if(inmethod){
				methodArray[methodpointer] +="\"" + node.child.toString() + "\"\n";
			}else{
				System.out.print('"');
				System.out.print(node.child.toString());
				System.out.print('"');
			}
			//Make sure it is printing out  the stuff between " and "
			return null;
		}

		@Override
		public Void visit(Breakpoint node) {
			if(inmethod){
				methodArray[methodpointer] +="// breakpoint printout\n";
				methodArray[methodpointer] +="for(int j = 0; j<30000; j++){ \n if(cell[j] != 0) \n System.out.println(\"Cell:\" + j +\" Contents:\" + array[j]); \n }";
			}else{
				
				System.out.println("// breakpoint printout");
				System.out.println("for(int j = 0; j<30000; j++){ \n if(cell[j] != 0) \n System.out.println(\"Cell:\" + j +\" Contents:\" + array[j]); \n }");
			}
			return null;
		}
		
	}
	
	public static class Interpreter implements Visitor<Void> {
		public interface Procedure {
		    void execute();
		}
		private Procedure[] procedures;
		byte[] cell;
		Queue<Character> input;
		int pointer;
        public interface Procedure {
            void execute();
        }
        private Procedure[] procedures;
		@Override
		public Void visit(Left node) {
			pointer--;
			return null;
		}
		@Override
		public Void visit(Right node) {
			pointer++;
			return null;
		}
		@Override
		public Void visit(Increment node) {
			cell[pointer]++;
			return null;
		}
		@Override
		public Void visit(Decrement node) {
			cell[pointer]--;
			return null;
		}
		@Override
		public Void visit(Input node) {
			if(!input.isEmpty()){
				if(input.peek() == null)
					cell[pointer] = 0;
				else
					cell[pointer] = (byte) input.poll().charValue();
			}else{
				try {
					cell[pointer] = (byte) System.in.read();
				} catch (IOException e) {
					// Seriously, Java?!?
				}
			}
			return null;
		}
		@Override
		public Void visit(Output node) {
			System.out.print((char)cell[pointer]);
			return null;
		}
		@Override
		public Void visit(ProcedureDefinition node) {
			// takes node an places it in a procedures array
			final Interpreter current =this;
			final ProcedureDefinition proc = node;
			
			procedures[cell[pointer]] = new Procedure(){

				@Override
				public void execute() {
					proc.child.accept(current);
				}
			};
			return null;
		}
		@Override
		public Void visit(Loop node) {
			while (cell[pointer] != 0) {
				node.child.accept(this);
			}
			return null;
		}
		@Override
		public Void visit(Program node) {
			cell = new byte[30000];
			pointer = 0;
			procedures = new Procedure[256];
			input = new LinkedList<Character>();
			node.child.accept(this);
			return null;
		}
		@Override
		
		public Void visit(Sequence node) {
			for (Node child : node.children) {
				child.accept(this);
			}
			return null;
		}
		@Override
		public Void visit(ProcedureInvocation node) {
			procedures[cell[pointer]].execute();
			return null;
		}
		@Override
		public Void visit(Repetition node) {
			for(int j = node.rep; j >0; j--){
				switch (node.command) {
				case '>': pointer++; break;
				case '<': pointer--; break;
				case '+': cell[pointer]++; break;
				case '-': cell[pointer]--; break;
				case '.': System.out.print((char)cell[pointer]); break;
				case ':': procedures[cell[pointer]].execute();
				case ',': if(!input.isEmpty()){
					if(input.peek() == null)
						cell[pointer] = 0;
					else
						cell[pointer] = (byte) input.poll().charValue();
					}else{
						try {
							cell[pointer] = (byte) System.in.read();
						} catch (IOException e) {
							// Seriously, Java?!?
						}
					} break;
				}
			}
			return null;
		}
		@Override
		public Void visit(rumString node) {
			char[] charArr = node.child.toCharArray();
			
			for(int i = 0; i<charArr.length; i++){
				input.add(charArr[i]);
			}
				input.add(null);
					
			return null;
		}
		@Override
		public Void visit(Breakpoint node) {
			for(int j = 0; j<30000; j++){
				if(cell[j] != 0)
					System.out.println("Cell:" + j +" Contents:" + cell[j]);
			}
			return null;
		}
	}
	int i = 0;
	public Sequence parseSequence(String source) {
		LinkedList<Node> sequence = new LinkedList<Node>();
		/* Consume one character at a time */
		int rep = 1;
		boolean stringbool = true;
		while (i < source.length()) {
			char command = source.charAt(i);
			i++;
			rep = 0;
			
			/* Add the proper Node for each command to the sequence */
			switch (command) {
			case '>': sequence.add(new Right(/* repetition */)); break;
			case '<': sequence.add(new Left()); break;
			case '+': sequence.add(new Increment()); break;
			case '-': sequence.add(new Decrement()); break;
			case '.': sequence.add(new Output()); break;
			case ',': sequence.add(new Input()); break;
			case '[': sequence.add(new Loop(parseSequence(source))); break;
			case ']': return new Sequence(sequence.toArray(new Node[0]));
			case '"': 
					String temp1 = "";
					do{
						command= source.charAt(i);
						if(command != '"')
							temp1+= command;
						i++;
					}while (command != '"');
					sequence.add(new rumString(temp1));
					break;
			case '(': sequence.add(new ProcedureDefinition(parseSequence(source))); break;
			case ')': return new Sequence(sequence.toArray(new Node[0])); 
			case ':': sequence.add(new ProcedureInvocation()); break;
			case '!': sequence.add(new Breakpoint()); break;
			}
			String temp ="";
			if( Character.isDigit(command)){
				while(Character.isDigit(command)&& i <source.length()){
					temp += command;
					
					command = source.charAt(i);
					i++;
				}
				rep = Integer.parseInt(temp);				
				if( command != '(' && command != '[' && command != '"' && command != ':' && command != '!'){
					sequence.add(new Repetition((rep), command));
				}
			}
			
		}
		return new Sequence(sequence.toArray(new Node[0]));
	}

	public Program parse (String source) {
		return new Program(parseSequence(opt(source)));
	}
	public static String opt(String source){
		String newsource ="";
		int count = 0;
		int oldcount = count;
		
		while(count < source.length()){
			while(count< source.length() && source.charAt(count) == source.charAt(oldcount)){
				count++;
			}
			if((count - oldcount) <2){
				newsource += source.charAt(oldcount);
				oldcount = count;
			}else{
				newsource += Integer.toString((count - oldcount));
				
				newsource += source.charAt(oldcount);
				oldcount = count;
			}
		}
		
		
		
		System.out.println(newsource.charAt(2));
		System.out.println(newsource);
		return newsource;
	}
	public static void main(String[] args) {
//		Program program = new Program(new Sequence(
//				new Increment(), new Loop(new Sequence(
//						new Output(), new Increment()))));
		Program program = new RUM().parse(">+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.");
		//Program program = new RUM().parse(">(<65+.>):");
		Interpreter interpreter = new Interpreter();
		program.accept(interpreter);
		System.out.println("");
		CompiletoJava compile = new CompiletoJava();
		//compile.visit(program);
		Printer printer = new Printer();
		printer.visit(program);
	}
	
}