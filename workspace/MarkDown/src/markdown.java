import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class markdown{

    public interface Node {
        public <T> T accept(Visitor<T> visitor);
    }


    public interface Visitor<T>{

        T visit(Bold node);
        T visit(Italic node);
        T visit(Strike node);
        T visit(Superscript node);
        T visit(Link node);
        T visit(Quote node);
        T visit(Code node);
        T visit(Bullets node);
        T visit(Numbers node);
        T visit(PTag node);
        T visit(Space node);
        T visit(Text node);
        T visit(Sequence node);

    }

    //Static Classes
    public static class PTag implements Node
    {


        private static PTag pTag = new PTag();
        private PTag() {}
        public static PTag getInstance(){
            return pTag;
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }


    }

    public static class Bold implements Node
    {
        Node child;
        private static HashMap<Node,Bold> map = new HashMap<Node,Bold>();
        private Bold(Node node)
        {
            this.child = node;
        }
        public static Bold getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Bold(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Italic implements Node
    {
        Node child;
        private static HashMap<Node,Italic> map = new HashMap<Node,Italic>();
        private Italic(Node node)
        {
            this.child = node;
        }
        public static Italic getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Italic(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Strike implements Node
    {
        Node child;
        private static HashMap<Node,Strike> map = new HashMap<Node,Strike>();
        private Strike(Node node)
        {
            this.child = node;
        }
        public static Strike getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Strike(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Superscript implements Node
    {
        Node child;
        private static HashMap<Node,Superscript> map = new HashMap<Node,Superscript>();
        private Superscript(Node node)
        {
            this.child = node;
        }
        public static Superscript getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Superscript(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Link implements Node
    {
        Node child;
        Text linkText;
        private static HashMap<ArrayList<Node>, Link> map = new HashMap<ArrayList<Node>, Link>();
        private static ArrayList<Node> list = new ArrayList<Node>();
        private Link(Node child,Text link)
        {
            this.child = child;
            this.linkText = link;
        }
        public static Link getInstance(Node child, Text link) {
            list.clear();
            list.add(child);
            list.add(link);
            if(!map.containsValue(list))
                map.put(list, new Link(child,link));
            return map.get(list);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Quote implements Node
    {
        Node child;
        private static HashMap<Node,Quote> map = new HashMap<Node,Quote>();
        private Quote(Node node){

            this.child = node;
        }
        public static Quote getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Quote(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Code implements Node
    {
        Node child;
        private static HashMap<Node,Code> map = new HashMap<Node,Code>();
        private Code(Node node)
        {
            this.child = node;
        }
        public static Code getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Code(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Bullets implements Node
    {
        Node child;
        private static HashMap<Node,Bullets> map = new HashMap<Node,Bullets>();
        private Bullets(Node node){

            this.child = node;
        }
        public static Bullets getInstance(Node node){
            if(!map.containsKey(node))
                map.put(node, new Bullets(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor)
        {
            return visitor.visit(this);
        }
    }

    public static class Numbers implements Node
    {
        Node child;
        private static HashMap<Node,Numbers> map = new HashMap<Node,Numbers>();
        String num;
        private Numbers(Node node,String num){

            this.child = node;
            this.num = num;
        }
        public static Numbers getInstance(Node node, String number){
            if(!map.containsKey(node))
            {
                map.put(node, new Numbers(node,number));
            }
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor)
        {
            return visitor.visit(this);
        }
    }

    public static class Text implements Node
    {
        String text;
        private static HashMap<String,Text> map = new HashMap<String,Text>();
        private Text(String text)
        {
            this.text = text;
        }
        public static Text getInstance(String node){
            if(!map.containsKey(node))
                map.put(node, new Text(node));
            return map.get(node);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }

    public static class Sequence implements Node {
        Node a,b;
        private static HashMap<ArrayList<Node>, Sequence> map = new HashMap<ArrayList<Node>, Sequence>();
        private static ArrayList<Node> list = new ArrayList<Node>();
        private Sequence(Node a, Node b) {
            this.a = a;
            this.b = b;
        }
        public static Sequence getInstance(Node a, Node b){
            // search for sequence if null create new sequence
            list.clear();
            list.add(a);
            list.add(b);
            if(!map.containsValue(list))
                map.put(list, new Sequence(a,b));
            return map.get(list);
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    public static class Space implements Node {
        private static Space space = new Space();
        private Space() {}
        public static Space getInstance(){
            return space;
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }


    //Printer
    public static class Printer implements Visitor<String> {
        @Override
        public String visit(Bold node) {
            return "<b>" + node.child.accept(this) + "</b>";
        }

        @Override
        public String visit(Italic node) {
            return "<i>" + node.child.accept(this) + "</i>";
        }

        @Override
        public String visit(Strike node) {
            return "<strike>" + node.child.accept(this) + "</strike>";
            //return null;
        }

        @Override
        public String visit(Superscript node) {
            return "<sup>" + node.child.accept(this) + "</sup>";
        }

        @Override
        public String visit(Link node) {
            return "<a href=\"" + node.linkText.accept(this) + "\">" + node.child.accept(this) + "</a>";       
        }

        @Override
        public String visit(Quote node) {

            return "<Font color = \"blue\">|</Font><Font color=\"Gray\" size=\"2\""+ node.child.accept(this) + "</Font>";
        }
        @Override
        public String visit(Code node) {
            return "<Font color=\"Gray\" &nbsp &nbsp &nbsp"+ node.child.accept(this) + "</Font>";
            //return null;
        }

        @Override
        public String visit(Bullets node) {
            return "<ul><li>" + node.child.accept(this)+"</li></ul>";
        }

        @Override
        public String visit(Numbers node) {
            return "<b>" + node.num + "</b>" + node.child.accept(this)  +"<br>";      
        }

        @Override
        public String visit(PTag node) {
            // TODO Auto-generated method stub
            return "<br />";
        }
        @Override
        public String visit(Space node) {
            return "&nbsp;";
            //return null;
        }

        @Override
        public String visit(Text node) {
            return node.text.toString();
        }

        @Override
        public String visit(Sequence node) {

            return node.a.accept(this) + node.b.accept(this) + "";
        }

    }


    public static Node treeBuilder(String source)
    {
        try{


            int i = 0;
            if(source.length() != 0)
            {
                char c = source.charAt(i);

                //Italics
                if(c=='*')    
                {
                    c = source.charAt(++i);
                    //Bold
                    if(c=='*')
                    {
                        c = source.charAt(++i);
                        //Bold Italics
                        if(c=='*')
                        {
                            c=source.charAt(++i);
                            String text=sub(source,i,"\\*\\*\\*");
                            i += text.length() +3;

                            return Sequence.getInstance(Bold.getInstance(Italic.getInstance(Text.getInstance(text))),treeBuilder(source.substring(i)));
                        }
                        else
                        {
                            System.out.print("");
                            //c=source.charAt(++i);
                            String text=sub(source,i,"\\*\\*");
                            i += text.length() +2;

                            return Sequence.getInstance(Bold.getInstance(treeBuilder(text)),treeBuilder(source.substring(i)));
                        }     
                    }
                    else
                    {
                        //c=source.charAt(++i);
                        String text=sub(source,i,"\\*");
                        i+= text.length()+1;

                        System.out.println("SUBString: "+source.substring(i));

                        return Sequence.getInstance(Italic.getInstance(Text.getInstance(text)),treeBuilder(source.substring(i)));
                    }
                }

                if (c == '~')
                {
                    c = source.charAt(++i);
                    if(c == '~')//Strike Encountered
                    {
                        c=source.charAt(++i);
                        String text=sub(source,i,"~~");
                        i += text.length()+1;

                        return Sequence.getInstance(Strike.getInstance(Text.getInstance(text)),treeBuilder(source.substring(i)));

                    }
                }

                if (c =='^')
                {
                    c=source.charAt(++i);
                    String text=sub(source,i," ");
                    i= text.length() +1;

                    return Sequence.getInstance(Superscript.getInstance(Text.getInstance(text)),treeBuilder(source.substring(i))); 
                }

                //Link
                if (c == '[')
                {
                    c=source.charAt(++i);
                    String text=sub(source,i,"]");
                    i+= text.length()+1;
                    //System.out.print(text);
                    c=source.charAt(i);
                    //System.out.print(c);
                    if (c == '(')
                    {

                        //System.out.print(link);
                        String link=sub(source,i,"\\)");
                        System.out.print(link);
                        i += link.length()+1;
                        System.out.print(link);
                        return Sequence.getInstance(Link.getInstance(treeBuilder(text),Text.getInstance(link)),treeBuilder(source.substring(i)));
                    }

                }

                if (c =='>')
                {
                    c=source.charAt(++i);
                    String text=sub(source,i,"\n");
                    i+= text.length()+1;
                    System.out.print("QUOTE:" + text);
                    return Sequence.getInstance(Quote.getInstance(treeBuilder(text)),treeBuilder(source.substring(i))); 
                }

                if(c ==' ')
                {
                    c = source.charAt(++i);
                    if(c ==' ')
                    {
                        c = source.charAt(++i);
                        if(c ==' ')
                        {
                            c = source.charAt(++i);
                            if(c ==' ')
                            {
                                c = source.charAt(++i);
                                String text=sub(source,i,"\n");
                                i +=text.length();
                                System.out.println(source.substring(i));

                                return Sequence.getInstance(Code.getInstance(Text.getInstance(text)),treeBuilder(source.substring(i))); 
                            }
                            else
                                return Sequence.getInstance(Space.getInstance(), treeBuilder(source.substring(i)));
                        }
                        else
                            return Sequence.getInstance(Space.getInstance(), treeBuilder(source.substring(i)));
                    }
                    else
                        return Sequence.getInstance(Space.getInstance(), treeBuilder(source.substring(i)));
                }

                //Check for bullets
                if(c == '(')
                {
                    c = source.charAt(++i);
                    if(c == 'b')
                    {
                        c = source.charAt(++i);
                        if(c == ')')
                        {
                            c = source.charAt(++i);
                            String text=sub(source,i,"\n");
                            i += text.length()+1;

                            return Sequence.getInstance(Bullets.getInstance(treeBuilder(text)),treeBuilder(source.substring(i))); 

                        }             
                    }
                }

                //Numbers
                if(Character.isDigit(c))
                {
                    StringBuilder sb= new StringBuilder();
                    while(Character.isDigit(c))
                    {
                        sb.append(c);

                        c = source.charAt(++i);
                    }
                    if(c =='.')
                    {
                        sb.append('.');
                        c = source.charAt(++i);
                        if(c == ' ')
                        {
                            sb.append(' ');
                            System.out.print("NUMBER:"+sb.toString());
                            String text=sub(source,i,"\n");
                            i += text.length();
                            return Sequence.getInstance(Numbers.getInstance(Text.getInstance(text),sb.toString()), treeBuilder(source.substring(++i)));
                        }
                        else return Sequence.getInstance(Text.getInstance(sb.toString()), treeBuilder(source.substring(i)));
                    }
                    else return Sequence.getInstance(Text.getInstance(sb.toString()+"."), treeBuilder(source.substring(i)));

                }


                //PTAG
                if(c=='\n')
                {
                    i++;
                    return Sequence.getInstance(PTag.getInstance(), treeBuilder(source.substring(i)));
                }

                /*String temp="";
            while(c != '\n' && i < source.length())
            {
                c=source.charAt(i);
                temp+=c;
                i++;
            }*/
                ArrayList<String> al=new ArrayList<String>();
                al.add("*");
                al.add("~");
                al.add("^");
                al.add(" ");
                al.add(">");
                //al.add("\n");
                StringBuilder sb= new StringBuilder();

                int begin=i;

                while(!al.contains(c+"") && i < source.length())
                {
                    i++;
                    if(i < source.length())
                        c=source.charAt(i);

                }

                if(begin != i)
                    sb.append(source.subSequence(begin,i));


                //Return empty leaf node
                if(i>=source.length())
                {
                    return Sequence.getInstance(Text.getInstance(sb.toString()),Text.getInstance(""));    
                }
                else
                    return Sequence.getInstance(Text.getInstance(sb.toString()),treeBuilder(source.substring(i)));    
            }
        }
        catch(Exception e)
        {
            //System.out.println("DEBUG:" +e.getMessage() + e.getStackTrace());

            return Text.getInstance("");
        }
        return Text.getInstance("");

    }


    public static String sub(String string,int startIndex,String endString)
    {
        String temp = string.substring(startIndex);

        String[] split = temp.split(endString);

        return split[0];    
    }


    public static void main(String[] args) {
        Printer printer = new Printer();
        //printer.vi
        Sequence seq2= Sequence.getInstance(treeBuilder("**TEST THIS** *Hello*(b)*BULLET*\n123.  Hello \n"),Text.getInstance(""));;

        Sequence seq= Sequence.getInstance(
                Bold.getInstance(Text.getInstance("WHAT")),Sequence.getInstance(Strike.getInstance(Text.getInstance("VHAT")),
                        Italic.getInstance(Text.getInstance("ITALICS"))));

        System.out.println(printer.visit(seq2));


        JOptionPaneTest.display("<html>*italics* <br>**bold**<br> ^Superscript</html>");

    }

    static class JOptionPaneTest {

        private static void display(String message) {

            JOptionPane jop = new JOptionPane();
            jop.setBounds(0,0,500,500);
            TextArea input = new TextArea();
            //

            JLabel output = new JLabel();
            output.setFont(new Font("serif", Font.PLAIN, 14)); 
            //output.setEnabled(false);
            input.setBounds(0, 0, 50, 200);
            JPanel panel = new JPanel(new GridLayout(0, 1));
            String[] buttons = {"Translate"};
            panel.add(input);
            panel.add(output);
            panel.setBounds(0, 0, 1000, 800);

            //panel.add(new JLabel(message));
            int result = 0;
            {

                while(result != -1)
                {

                    result = JOptionPane.showOptionDialog(
                            null,
                            panel,
                            "Markdown Translator",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.PLAIN_MESSAGE,
                            null, buttons, result
                            );


                    if (result == 0) {
                        Printer printer = new Printer();
                        //printer.vi
                        Sequence seq2= Sequence.getInstance(treeBuilder(input.getText()),Text.getInstance(""));;

                        output.setText("<html><body>"+printer.visit(seq2)+"</body></html>");

                    } else {
                    			//nothing, You get nothing!!!!
                    }
                }
            }
        }
    }
}


