import java.util.ArrayList;
import java.util.HashMap;


public class Regex {
	public interface Node {
		public <T> T accept(Visitor<T> visitor);
	}
	public interface Visitor<T> {
		T visit(EmptySet node);
		T visit(EmptyString node);
		T visit(Symbol node);
		T visit(Star node);
		T visit(Sequence node);
		T visit(Or node);
	}
	// Item 1:
	 public static class Printer implements Visitor<String> {
		 
         @Override
         public String visit(EmptySet node) {
                 return "∅";
         }

         @Override
         public String visit(EmptyString node) {
                 return "ε";
         }

         @Override
         public String visit(Symbol node) {
                 return node.symbol+"";
                 //return null;
         }

         @Override
         public String visit(Star node) {
                 return node.child.accept(this)+"*";
         }

         @Override
         public String visit(Sequence node) {
                  return node.a.accept(this)+
                  node.b.accept(this);
                  
                  
         }

         @Override
         public String visit(Or node) {
                 // TODO Auto-generated method stub
                 return "["+node.a.accept(this)+"|"+ node.b.accept(this)+"]";
         }

 }
	// Reject everything
	// FIXME: Singleton: only one instance of this class
	public static class EmptySet implements Node {
		private static EmptySet emptySet = new EmptySet();
		private EmptySet() {}
		public static EmptySet getInstance() {
			return emptySet;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Matches "" Accept the end of a string
	// FIXME: Singleton    DONE
	public static class EmptyString implements Node {
		private static EmptyString emptyString = new EmptyString();
		private EmptyString() {}
		public static EmptyString getInstance(){
			return emptyString;
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Match a single symbol
	// FIXME: Flyweight  DONE
	public static class Symbol implements Node {
		char symbol;
		private static HashMap<Character, Symbol> map = new HashMap<Character, Symbol>();
		// It's private, as in, do not use outside this class
		private Symbol (char symbol) {
			this.symbol = symbol;
		}
		// How we actually "construct" a symbol
		public static Symbol getInstance(char symbol) {
			if (!map.containsKey(symbol)) {
				map.put(symbol, new Symbol(symbol));
			}
			return map.get(symbol);
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Match (child)*
	// FIXME: Flyweight  DONE
	// FIXME: Compaction
	public static class Star implements Node {
		private static HashMap<Node, Star> map = new HashMap<Node, Star>();
		Node child;
		private Star(Node child) {
			this.child = child;
		}
		// if the child is an emptyString, return emptyString
		public static Node getInstance(Node child) {
			// Compaction (don't bother creating junk)
			if (child == EmptySet.getInstance())
				return child;
			if (!map.containsKey(child))
				map.put(child, new Star(child));
			return map.get(child); 
		}
		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Match a followed by b
	// FIXME: Flyweight DONE
	// FIXME: Compaction
	public static class Sequence implements Node {
		private static HashMap<String,Sequence> map = new HashMap<String,Sequence>();
        Node a, b;
        private Sequence(Node a, Node b) {
                this.a = a; this.b = b;
        }
        
        public static Node getInstance(Node a,Node b) {
                // Compaction (don't bother creating junk)
                // A and EmptySet = EmptySet
                if(a.toString() == EmptySet.getInstance().toString() ||b.toString() == EmptySet.getInstance().toString())
                    return EmptySet.getInstance();
                    
                //return new Star(child); // Use the flyweight pattern here
                if(!map.containsKey(a.toString()+b.toString())){
                        map.put(a.toString()+b.toString(), new Sequence(a,b));
                }
                        
                return map.get(a.toString()+b.toString());
        }

		@Override
		public <T> T accept(Visitor<T> visitor) {
			return visitor.visit(this);
		}
	}
	// Match a or b
	// FIXME: Flyweight DONE
	// FIXME: Compaction
	public static class Or implements Node {
		private static HashMap<String,Or> map=new HashMap<String,Or>();
        Node a, b;
        private Or(Node a, Node b) {
                this.a = a; this.b = b;
        }
        public static Node getInstance(Node a,Node b) {
                // Compaction (don't bother creating junk)
                 // B OR EMPTY = B.
                if(a.toString() == EmptySet.getInstance().toString())
                    map.put(b.toString(), new Or(b,EmptySet.getInstance()));
                else if(b.toString() == EmptySet.getInstance().toString())
                    map.put(a.toString(), new Or(a,EmptySet.getInstance()));
                    
                if(!map.containsKey(a.toString()+b.toString())){
                        map.put(a.toString()+b.toString(), new Or(a,b));
                }
                        
                return map.get(a.toString()+b.toString());
        }
        @Override
        public <T> T accept(Visitor<T> visitor) {
                return visitor.visit(this);
                
        }

	}
	// Rewrite the regex to match
	// the rest of a string without the first char c.
	// We shouldn't use the new operator in the visit methods
	public static class Derivative implements Visitor<Node> {
		Nullable nullable = new Nullable();
		public char c; // Derive with respect to c
		@Override
		public Node visit(EmptySet node) {
			// Dc(0) = 0
			return node;
		}
		@Override
		public Node visit(EmptyString node) {
			// Dc("") = 0
			return EmptySet.getInstance();
		}
		@Override
		public Node visit(Symbol node) {
			// Dc(c) = ""
			if (c == node.symbol)
				return EmptyString.getInstance(); // Do the same thing for the empty string
			// Dc(c') = 0 if c is not c'
			else
				return EmptySet.getInstance();
		}
		@Override
		public Node visit(Star node) {
			// Dc(a*) = Dc(a)a*
			return  Sequence.getInstance(node.child.accept(this), node);
		}

		@Override
		public Node visit(Sequence node) {
			Node result = Sequence.getInstance(node.a.accept(this), node.b);
			// Dc(AB) = Dc(A)B if A does not contain the empty string
			if (!node.a.accept(nullable)) {
				return result;
			// Dc(AB) = Dc(A)B | Dc(B) if A contains the empty string
			} else {
				return Or.getInstance(
						result, // Dc(AB)
						node.b.accept(this) // Dc(B)
						);
			}
		}
		@Override
		public Node visit(Or node) {
			// Dc(A | B) = Dc(A) | Dc(B)
			return Or.getInstance(node.a.accept(this), node.b.accept(this));
		}
	}
	// Does the regex match the empty string?
	public static class Nullable implements Visitor<Boolean> {
		@Override
		public Boolean visit(EmptySet node) {
			return false;
		}
		@Override
		public Boolean visit(EmptyString node) {
			return true;
		}
		@Override
		public Boolean visit(Symbol node) {
			return false;
		}
		@Override
		public Boolean visit(Star node) {
			return true;
		}
		@Override
		public Boolean visit(Sequence node) {
			return node.a.accept(this) && node.b.accept(this);
		}
		@Override
		public Boolean visit(Or node) {
			return node.a.accept(this) || node.b.accept(this);
		}
	}
	// Use derivatives to match regular expressions
	public static boolean match(Node regex, String string) {
		// Two visitors
		Derivative d = new Derivative();
		Nullable nullable = new Nullable();
		// For debugging, create the printer here
		Printer printer = new Printer();

		// Just compute the derivative with respect to the first character, then the second, then the third and so on.
		for (char c : string.toCharArray()) {
			d.c = c; // Set the first character
			// For debugging purposes,
			// Print out the new regex
			System.out.println(regex.accept(printer));
			regex = regex.accept(d); // regex should match what it used to match, sans first character c
		}
		// If the final language contains the empty string, then the original string was in the original language.
		// Does the regex match the empty string?
		return regex.accept(nullable);
	}
	public static Node produceRegex(String regexString){
		// takes in string and creates a Regex for it.
		// parses string and returns Regex
		
		
		//check if there is a string
		//check if there is a character
		//	look if at next position
		//		if character or '('
		//			do something
		//		if or
		//			look ahead
		//				do stuff
		//		if *
		//			do stuff
		//check if there is a '('
		//	collect until ')'
		//		check for *
		//			do stuff
		//		check for or
		//			do stuff
		//the return that should never reach
		
		
		
		int count = 0;
		char currentletter;
		char finalletter;
		String remaining;
		String collected = "";
		String collected2 = "";
		try{
			Character.isLetter(regexString.charAt(count));
		}catch(IndexOutOfBoundsException e){
			//There is not string at all, return empty string
			return EmptyString.getInstance();
		}
		if(Character.isLetter(regexString.charAt(count))){
			currentletter = regexString.charAt(count);
			count++;
			try{
				if(Character.isLetter(regexString.charAt(count)) || regexString.charAt(count) == '('){
					//found a character next
					remaining = regexString.substring(count);
					return Sequence.getInstance(Symbol.getInstance(currentletter), produceRegex(remaining));
					
					//end if letter or '('
				}else if(regexString.charAt(count) == '|'){
					//found and or symbol
					// look ahead again
					while(true){
						try{
							count++;
							if(Character.isLetter(regexString.charAt(count))){
								finalletter = regexString.charAt(count);
								count++;
								try{
									remaining = regexString.substring(count);
									return Sequence.getInstance(Or.getInstance(Symbol.getInstance(currentletter), Symbol.getInstance(finalletter)), produceRegex(remaining));
								}catch(IndexOutOfBoundsException e){
									// Something went wrong, just return the OR
									return Or.getInstance(Symbol.getInstance(currentletter), Symbol.getInstance(finalletter));
								}
							}else if(regexString.charAt(count) == '('){
								count++;
								try{
									while(regexString.charAt(count) == ')'){
										collected += regexString.charAt(count);
										count++;
									}
								}catch(IndexOutOfBoundsException e){
									// Something went wrong, forget all after OR symbol and return current letter symbol
									return Symbol.getInstance(currentletter);
								}
								count++;
								try{
									remaining = regexString.substring(count);
									return Sequence.getInstance(Or.getInstance(Symbol.getInstance(currentletter), produceRegex(collected)), produceRegex(remaining));
								}catch(IndexOutOfBoundsException e){
										// Something went wrong, just return the OR
										return Or.getInstance(Symbol.getInstance(currentletter), produceRegex(collected));
								}
								
							}
						}catch(IndexOutOfBoundsException e){
							// nothing after OR symbol, just return current letter symbol
							return Symbol.getInstance(currentletter);
						}
					}
					//end else if OR
				}else if(regexString.charAt(count) == '*'){
					count++;
					try{
						remaining = regexString.substring(count);
						return Sequence.getInstance(Star.getInstance(Symbol.getInstance(currentletter)), produceRegex(remaining));
					}catch(IndexOutOfBoundsException e){
						// Something went wrong, just return the star
						return Star.getInstance(Symbol.getInstance(currentletter));
					}
					//end else if star
				}
			}catch(IndexOutOfBoundsException e){
				//something went wrong, just return symbol
				return Symbol.getInstance(currentletter);
			}
			//end if letter as first symbol
		}else if (regexString.charAt(count) == '('){
			count++;
			try{
				while(regexString.charAt(count) == ')'){
					collected += regexString.charAt(count);
					count++;
				}
			}catch(IndexOutOfBoundsException e){
				// Something went wrong, forget everything return empty
				return EmptyString.getInstance();
			}
			count++;
			//check if star
			try{
				if (regexString.charAt(count) == '*'){
					return Star.getInstance(produceRegex(collected));
				}
			}catch(IndexOutOfBoundsException e){
				//something went wrong, end of string
				return produceRegex(collected);
			}//not star
			//there is something above at this point or it would have not made it this far
			if (regexString.charAt(count) == '|'){
				//copy pasta with new sauce OR 
				while(true){
					try{
						count++;
						if(Character.isLetter(regexString.charAt(count))){
							finalletter = regexString.charAt(count);
							count++;
							try{
								remaining = regexString.substring(count);
								return Sequence.getInstance(Or.getInstance(produceRegex(collected), Symbol.getInstance(finalletter)), produceRegex(remaining));
							}catch(IndexOutOfBoundsException e){
								// Something went wrong, just return the OR
								return Or.getInstance(produceRegex(collected), Symbol.getInstance(finalletter));
							}
						}else if(regexString.charAt(count) == '('){
							count++;
							try{
								while(regexString.charAt(count) == ')'){
									collected2 += regexString.charAt(count);
									count++;
								}
							}catch(IndexOutOfBoundsException e){
								// Something went wrong, forget all after OR symbol and return current letter symbol
								return produceRegex(collected);
							}
							count++;
							try{
								remaining = regexString.substring(count);
								return Sequence.getInstance(Or.getInstance(produceRegex(collected), produceRegex(collected2)), produceRegex(remaining));
							}catch(IndexOutOfBoundsException e){
									// Something went wrong, just return the OR
									return Or.getInstance(produceRegex(collected), produceRegex(collected2));
							}
							
						}
					}catch(IndexOutOfBoundsException e){
						// nothing after OR symbol, just return current letter symbol
						return produceRegex(collected);
					}
				}
				//end if or
			}
			try{
				remaining = regexString.substring(count);
				return Sequence.getInstance(produceRegex(collected), produceRegex(remaining));
			}catch(IndexOutOfBoundsException e){
				//something went wrong, end of string
				return produceRegex(collected);
			}
			//end '('
		}
		
		//This should never reach here
		return EmptyString.getInstance();
	}
	public static void main(String[] args) {
		String s = "H";
		s += "ello";
		if("Hello" == (s)) {
			System.out.println("WTF");
		}
		// Does a|b match a?
		long then = System.nanoTime();
		for (int i = 0; i < 1; i++)
			Regex.match(
				produceRegex("ba|ve"),"ve");
		System.out.println(System.nanoTime() - then);
	}
}
